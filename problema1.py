import os
from typing import Dict, List
from hashlib import md5


def get_file_hash(filename: str) -> hash:
    ficheiro = open(filename, "rb")
    # melhorar para ler bloco a bloco em vez de na totalidade
    # poderá falhar para ficheiros muito grandes
    conteudo = ficheiro.read()
    file_hash = md5(conteudo)
    ficheiro.close()
    return file_hash


def list_files(root_folder: str = ".\\temp") -> List[str]:
    temp = []
    for root, directories, files in os.walk(root_folder):
        for name in files:
            temp.append(os.path.join(root, name))
    return temp


def get_unique_list(origem: list) -> dict:
    unique = {}
    for ficheiro in origem:
        file_id: hash = get_file_hash(ficheiro)
        md5str: str = file_id.hexdigest()

        if md5str in unique:
            unique[md5str].append(ficheiro)
        else:
            unique[md5str] = [ficheiro]
    return unique


def delete_files(duplic: dict, csv: str = "apagados.csv"):
    ficheiro = open(csv, "w")

    for dupe in duplic:
        print(dupe)
        ficheiro.write(dupe+"\n")
        count = 1
        for f in duplic[dupe]:
            ficheiro.write(str(count)+","+duplic[dupe][count-1]+"\n")
            print(count, ",", duplic[dupe][count-1])
            if count > 1:
                print("*** apagar este ficheiro!")
            count = count+1
    ficheiro.close


ficheiros: list = list_files()
duplicados: dict = get_unique_list(ficheiros)

delete_files(duplicados)
